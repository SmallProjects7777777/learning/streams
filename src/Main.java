import java.io.IOException;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        Data data = new Data();
        List<String> readLinesFromFile = data.getLines();

        List<String[]> list = readLinesFromFile.stream()
                .skip(1)
                .map(line -> line.split(";"))
                .toList();


        //1. Wyświetl komentarze w kolejności chronologicznej
//        List<String[]> sortedByDate = list.stream()
//                .sorted(Comparator.comparing(comment -> LocalDate.parse(comment[2])))
//                .toList();
//        sortedByDate.forEach(comment -> System.out.println(comment[1] + " : " + comment[2]));


        //2. Wyświetl tylko te komentarze, które mają więcej niż 10 polubień
//        list.stream()
//                .filter(item -> Integer.parseInt(item[3]) > 10)
//                .forEach(comment -> System.out.println(comment[1]));

        //3. Znajdź najdłuższy oraz najkrótszy komentarz
//        String shortestComment = list.stream()
//                .skip(1)
//                .map(comment -> comment[1])
//                .min(Comparator.comparingInt(String::length))
//                .get();
//        System.out.println(shortestComment);
//
//        String longestComment = list.stream()
//                .skip(1)
//                .map(comment -> comment[1])
//                .max(Comparator.comparingInt(String::length))
//                .get();
//        System.out.println(longestComment);

        //4. Oblicz średnią liczbę polubień dla wszystkich komentarzy.
//
//        Double avgNumber = list.stream()
//                .collect(Collectors.averagingDouble(comment -> Integer.parseInt(comment[3])));
//        System.out.println(avgNumber);

        //5. Oblicz ile komentarzy zostało dodanych przez mężczyzn i kobiety.

//        long woman = list.stream()
//                .filter(comment -> comment[0].endsWith("a"))
//                .count();
//        System.out.println(woman);
//
//        long man = list.stream()
//                .filter(comment -> !comment[0].endsWith("a"))
//                .count();
//        System.out.println(man);

//      //6. Oblicz łączną liczbę polubień dla wszystkich komentarzy
//        int sum = list.stream().mapToInt(comment -> Integer.parseInt(comment[3])).sum();
//        System.out.println(sum);

        //7. Znajdź wszystkich użytkowników, którzy zostawili komentarze o długości przekraczającej 50 znaków
//        list.stream()
//                .filter(comment -> comment[1].length() > 50)
//                .forEach(user -> System.out.println(user[0]));

        //8. Posortuj komentarze według liczby polubień w kolejności malejącej.
//        list.stream()
//                .sorted(Comparator.comparing((String[] comment) -> Integer.parseInt(comment[3])).reversed())
//                .forEach(user -> System.out.println(user[0]));

        //9. Wyświetl komentarze dodane w październiku 2023.
//        list.stream()
//                .filter(comment -> LocalDate.parse(comment[2]).getMonthValue() == 10 && LocalDate.parse(comment[2]).getYear() == 2023)
//                .forEach(user -> System.out.println(user[0]));

        //10. Wyświetl unikalne daty, w których zostawiono komentarze.
//        list.stream()
//                .map(date -> date[2])
//                .distinct()
//                .forEach(System.out::println);

        //11. Oblicz sumę polubień dla komentarzy zostawionych w listopadzie 2023.
//        int sum = list.stream()
//                .filter(comment -> LocalDate.parse(comment[2]).getMonthValue() == 11 && LocalDate.parse(comment[2]).getYear() == 2023)
//                .mapToInt(number -> Integer.parseInt(number[3])).sum();
//        System.out.println(sum);

        //12. Wyświetl wszystkie unikalne imiona użytkowników.
//        list.stream()
//                .map(user -> user[0])
//                .distinct()
//                .forEach(System.out::println);

        //13. Oblicz średnią długość komentarzy.
//        Double avgCollect = list.stream()
//                .collect(Collectors.averagingDouble(comment -> comment[1].length()));
//        System.out.println(avgCollect);

        //14. Oblicz średnią liczbę słów na komentarz.
//        double avgWords = list.stream()
//                .mapToInt(comment -> comment[1].split("[\\s.,!]+").length)
//                .average()
//                .orElse(0);
//
//        System.out.println(avgWords);

        //15. Wyświetl komentarze, które zawierają przynajmniej jedno słowo zaczynające się na literę "s".
//        list.stream()
//                .filter(comment -> Arrays.stream(comment[1].split("[\\s.,!]+"))
//                        .anyMatch(strings -> strings.startsWith("a")))
//                .map(trueComment -> trueComment[1])
//                .forEach(System.out::println);

        //16. Wyświetl komentarze użytkowników, których imiona zawierają literę "k" (bez względu na wielkość liter).
//        list.stream()
//                .filter(user -> user[0].toLowerCase().contains("k"))
//                .forEach(comment -> System.out.println(comment[1]));

        //17. Posortuj komentarze według długości komentarza, a następnie wybierz 5 najdłuższych.
//        list.stream()
//                .sorted(Comparator.comparing((String[] comment) -> comment[1].length()).reversed())
//                .limit(5)
//                .forEach(comment -> System.out.println(comment[1]));

        //18. Posortuj komentarze alfabetycznie według nazw użytkowników, a następnie wybierz 5 pierwszych komentarzy w alfabetycznym porządku.
//        list.stream()
//                .sorted(Comparator.comparing(user -> user[0]))
//                .map(comment -> comment[1])
//                .limit(5)
//                .forEach(System.out::println);

        //19. Znajdź wszystkich użytkowników, którzy zostawili komentarze o długości przekraczającej średnią długość komentarzy.
//        Double collect = list.stream()
//                .collect(Collectors.averagingDouble(user -> user[1].length()));
//        System.out.println(collect);
//
//        list.stream()
//                .filter(user -> user[1].length() > collect)
//                .map(user -> user[0])
//                .forEach(System.out::println);

        //20. Znajdź datę, w której zostawiono najwięcej komentarzy.
//        Map<String, Long> collect = list.stream()
//                .collect(Collectors.groupingBy(user -> user[2], Collectors.counting()));
//
//        Map.Entry<String, Long> listMax = collect.entrySet().stream()
//                .max(Map.Entry.comparingByValue())
//                .orElse(null);
//        System.out.println(listMax.getKey());


    }
}