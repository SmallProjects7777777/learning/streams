import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Data {

    public List<String> getLines() throws IOException {
        Path path = getPath();
        return Files.readAllLines(path);
    }

    private Path getPath() {
        return Paths.get("C:\\Users\\Damian\\Documents\\Java\\StreamsLearn\\src\\dane.txt");
    }
}
